package com.carbonIT;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args)  {
        // path input file
        String pathFile = Paths.get("").toAbsolutePath().toString()+"/src/com/carbonIT/Input.txt";
        // initialize game
        Game game= new Game();
        game = initialize(pathFile,game);
        game.setupMap();

        // launch the game
        int nbrRounds = game.maxRound();
        for(int i =0;i<nbrRounds;i++){
            System.out.println("Round "+(i+1));
            game.round();
            game.displayMap();
        }
        // export results in the file Output.txt
        game.output();
    }
    /**
     * Get information from the file to initialise the game
     *
     * @param pathFile path file (input)
     * @param g game
     * @return game initialized with input information
     */
    public static Game initialize(String pathFile, Game g){
        String[][] map;
        ArrayList<Mountain> list_mountain = new ArrayList<>();
        ArrayList<Treasure> list_Treasure = new ArrayList<>();
        ArrayList<Adventurer> adventurer_list= new ArrayList<>();;

        try {
            // Read File
            File file = new File(pathFile);
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            String line;
            adventurer_list = new ArrayList<Adventurer>();
            int x,y,nbr;
            while((line = br.readLine()) != null){
                if(line.charAt(0)!='#'){
                    //delete space
                    line = line.replaceAll(" ","");

                    // initialize map
                    if(line.charAt(0)=='C'){
                        int width = Integer. parseInt(line.substring(line.indexOf('-')+1, line.lastIndexOf('-')));
                        int height = Integer. parseInt(line.substring(line.lastIndexOf('-')+1,line.length()));
                        map = new String[height][width];
                        Arrays.stream(map).forEach(a -> Arrays.setAll(a, i -> "."));
                        g.setMap(map);
                    }
                    // Add the Mountain in the map
                    if(line.charAt(0)=='M'){

                        x = Integer. parseInt(line.substring(line.indexOf('-')+1, line.lastIndexOf('-')));
                        y = Integer. parseInt(line.substring(line.lastIndexOf('-')+1,line.length()));

                        list_mountain.add(new Mountain(x,y));
                    }
                    // Add the Treasures in the map
                    else if(line.charAt(0)=='T'){
                        String coord = line.substring(line.indexOf('-')+1, line.lastIndexOf('-'));
                        x = Integer. parseInt(coord.substring(0, coord.lastIndexOf('-')));
                        y = Integer. parseInt(coord.substring(coord.lastIndexOf('-')+1,coord.length()));
                        nbr = Integer. parseInt(line.substring(line.lastIndexOf('-')+1,line.length()));

                        list_Treasure.add(new Treasure(x,y,nbr));
                    }
                    else if(line.charAt(0)=='A') {
                        // get movement
                        String seq = new String(line.substring(line.lastIndexOf('-') + 1, line.length()));

                        // get orientation
                        String tmp;
                        tmp = line.substring(0, line.length() - seq.length() - 1);
                        String orientation = new String(tmp.substring(tmp.lastIndexOf('-') + 1, tmp.length()));

                        // get x and y
                        tmp = tmp.substring(0, tmp.lastIndexOf('-'));
                        y = Integer.parseInt(tmp.substring(tmp.lastIndexOf('-') + 1, tmp.length()));
                        tmp = tmp.substring(0, tmp.lastIndexOf('-'));
                        x = Integer.parseInt(tmp.substring(tmp.lastIndexOf('-') + 1, tmp.length()));

                        // get name
                        tmp = tmp.substring(0, tmp.lastIndexOf('-'));
                        String name = new String(tmp.substring(tmp.lastIndexOf('-') + 1, tmp.length()));

                        adventurer_list.add(new Adventurer(name, x, y, orientation, seq));
                    }
                }
            }
            g.setAdventurer_list(adventurer_list);
            g.setList_Treasure(list_Treasure);
            g.setList_mountain(list_mountain);
        }catch(Exception e){
            e.printStackTrace();
        }
        return g;
    }
}
