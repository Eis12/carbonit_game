package com.carbonIT;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Game {

    private String[][] map;
    private ArrayList<Mountain> list_mountain = new ArrayList<>();
    private ArrayList<Treasure> list_Treasure = new ArrayList<>();
    private ArrayList<Adventurer> adventurer_list;

    public Game(){
    }
    /**
     * Set up map (Add mountains, treasures and adventurers to the map)
     */
    public void setupMap(){

        if(map!=null){

            // add mountain to the map
            for(Mountain m : list_mountain){
                this.map[m.getY()][m.getX()]="M";
            }
            // add treasure to the map
            for(Treasure t : list_Treasure){
                this.map[t.getY()][t.getX()]="T("+t.getNbr()+")";;
            }
            for(Adventurer a : adventurer_list){
                this.map[a.getY()][a.getX()]="A("+a.getName()+")";
            }
        }
    }
    /**
     * display the map (mountains, treasures and adventurers...)
     */
    public void displayMap(){

        //print map
        for (int y=0;y<map.length;y++) {
            for (int x = 0; x <map[0].length ; x++)
                System.out.print(this.map[y][x] + " ");
            System.out.print("\n");
        }

        for(Adventurer a : this.adventurer_list){
            System.out.println(a);
        }
    }

    /**
     * one round = one movement for each adventurer
     */
    public void round(){
        char mouv;
        Game g;
        for (Adventurer a:adventurer_list) {
            if (!a.getSeq_mov().isEmpty()) {
                mouv = a.getSeq_mov().charAt(0);
                if (mouv == 'D' || mouv == 'G') {
                    a.changeOrientation();
                } else if (mouv == 'A') {
                    g = a.moveAhead(this);
                    this.setMap(g.getMap());
                    this.setList_Treasure(g.list_Treasure);
                    this.setAdventurer_list(g.adventurer_list);
                }
            }
        }
    }
    /**
     * export results in the file "Output.txt"
     */
    public void output() {
        try {
            FileWriter fileWriter = new FileWriter("./src/com/carbonIT/Output.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            printWriter.print("C - "+this.map[0].length+" - "+this.map.length+"\n");

            for(Mountain m : list_mountain){
                printWriter.print(m + "\n");
            }

            printWriter.print("# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants} \n");
            for(Treasure t : list_Treasure){
                printWriter.print(t + "\n");

            }
            printWriter.print("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés} \n");
            for(Adventurer a : adventurer_list){
                printWriter.print(a + "\n");
            }

            printWriter.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public int maxRound(){
        int max =0;
        for(Adventurer a : adventurer_list){
            if(a.getSeq_mov().length()>max){
                max = a.getSeq_mov().length();
            }
        }
        return max;
    }

    public String[][] getMap() {
        return map;
    }

    public void setMap(String[][] map) {
        this.map = map;
    }

    public ArrayList<Mountain> getList_mountain() {
        return list_mountain;
    }

    public void setList_mountain(ArrayList<Mountain> list_mountain) {
        this.list_mountain = list_mountain;
    }

    public ArrayList<Treasure> getList_Treasure() {
        return list_Treasure;
    }

    public void setList_Treasure(ArrayList<Treasure> list_Treasure) {
        this.list_Treasure = list_Treasure;
    }

    public ArrayList<Adventurer> getAdventurer_list() {
        return adventurer_list;
    }

    public void setAdventurer_list(ArrayList<Adventurer> adventurer_list) {
        this.adventurer_list = adventurer_list;
    }
}



