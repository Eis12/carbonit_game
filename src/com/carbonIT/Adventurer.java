package com.carbonIT;

import java.util.ArrayList;

public class Adventurer {
    private String name;
    private int x;
    private int y;
    private String orientation;
    private String seq_mov;
    private int nbr_treasure;

    public Adventurer(String name, int x, int y, String orientation, String seq_mouv) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.seq_mov = seq_mouv;
        nbr_treasure=0;
    }

    /**
     * Change the adventure's orientation
     */
    public void changeOrientation(){
        if(this.seq_mov.charAt(0)=='D'){
            if(this.orientation.equals("N")){
                this.orientation="E";
            }
            else if(this.orientation.equals("S")){
                this.orientation="O";
            }
            else if(this.orientation.equals("E")){
                this.orientation="S";
            }
            // else Ouest orientation
            else{
                this.orientation="N";
            }
        }else{
            if(this.orientation.equals("N")){
                this.orientation="O";
            }
            else if(this.orientation.equals("S")){
                this.orientation="E";
            }
            else if(this.orientation.equals("E")){
                this.orientation="N";
            }
            // else Ouest orientation
            else {
                this.orientation = "S";
            }
        }

        //remove the movement from the sequence
        this.seq_mov = this.seq_mov.substring(1);

    }
    /**
     * Allow the adventurers to move ahead
     * @param game It's the current game
     * @return game updated
     */
    public Game moveAhead(Game game){

        String[][] map = game.getMap();
        ArrayList<Treasure> listT = game.getList_Treasure();

        if(this.orientation.equals("N")){
            //y will change if the new position is not outside the map, there's no Mountain and Adventurer
            if(this.y-1>=0 && map[this.y-1][this.x]!="M"&& !map[this.y-1][this.x].contains("A")){
                // update the current position in the map
                map = updateCurrentPosition(map, x,y,listT);
                //update the position
                this.y = y-1;

                game = updateNewPosition(game,x,y);
            }
        }
        else if(this.orientation.equals("S")){

            if(this.y+1<map.length && map[this.y+1][this.x]!="M"&& !map[this.y+1][this.x].contains("A")){
                // update the current position in the map
                map = updateCurrentPosition(map, x,y,listT);
                //update the position
                this.y = y+1;

                game = updateNewPosition(game, x, y);

            }
        }
        else if(this.orientation.equals("O")){
            if(this.x-1>=0 && map[this.y][this.x-1]!="M"&& !map[this.y][this.x-1].contains("A")){
                // update the current position in the map
                map = updateCurrentPosition(map, x,y,listT);
                //update the position
                this.x = x-1;
                game = updateNewPosition(game, x, y);
            }
        }
        else if(this.orientation.equals("E")){
            if(this.x+1<map[0].length && map[this.y][this.x+1]!="M"&& !map[this.y][this.x+1].contains("A")){
                // update the current position in the map
                map = updateCurrentPosition(map, x,y,listT);
                //update the position
                this.x = x+1;
                game = updateNewPosition(game, x, y);
            }
        }

        //remove the movement from the sequence
        this.seq_mov = this.seq_mov.substring(1);
        return game;
    }

    //Update the current position in the map after the adventurer move to the next position
    public String[][] updateCurrentPosition(String[][] map, int x, int y, ArrayList<Treasure> listT){
        //If there is a treasure in the current position
        if(map[y][x].contains("T")){
            // get the treasure in the current position
            Treasure t = listT.stream().filter(treasure -> (treasure.getX()==this.x && treasure.getY()==this.y )).findAny().orElse(null);
            // indicate it in the map
            map[y][x]=  "T(" + t.getNbr() + ")";
        }else {
            // no treasure in the current position
            map[y][x] = ".";
        }
        return map;
    }

    //Update the new position in the map and update the number of Treasure (if needed) and the number of treasure found by the adventurer (if needed)
    public Game updateNewPosition(Game game, int x, int y){
        ArrayList<Treasure> listT = game.getList_Treasure();
        String[][] map = game.getMap();
        // If the Adventurer find a treasure in the new position
        Treasure t = listT.stream().filter(treasure -> (treasure.getX()==this.x && treasure.getY()==this.y )).findAny().orElse(null);
        if(t!=null){
            // decrease the number of treasure
            t.setNbr(t.getNbr()-1);
            // increase the number of treasures found by the adventurer
            this.nbr_treasure += 1;
            // indicate in the map the adventurer and the treasure (if there's still a treasure)
            if(t.getNbr()!=0) {
                map[y][x] = "A(" + name + ")-T(" + t.getNbr() + ")";
            }else{
                map[y][x] = "A(" + name + ")";
            }
        }
        // if there's no treasure
        else {
            map[y][x] = "A(" + name + ")";
        }
        game.setMap(map);
        game.setList_Treasure(listT);
        return game;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public String getSeq_mov() {
        return seq_mov;
    }

    public void setSeq_mov(String seq_mov) {
        this.seq_mov = seq_mov;
    }

    public int getNbr_treasure() {
        return nbr_treasure;
    }

    public void setNbr_treasure(int nbr_treasure) {
        this.nbr_treasure = nbr_treasure;
    }

    @Override
    public String toString() {
        return "A - "+name+" - "+x+" - "+y+" - "+orientation+" - "+nbr_treasure ;
    }


}
