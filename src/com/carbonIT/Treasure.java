package com.carbonIT;

public class Treasure {
    private int x;
    private int y;
    private int nbr;

    public Treasure(int x, int y, int nbr) {
        this.x = x;
        this.y = y;
        this.nbr = nbr;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getNbr() {
        return nbr;
    }

    public void setNbr(int nbr) {
        this.nbr = nbr;
    }

    @Override
    public String toString() {
        return "T - " + x +" - "+ y +" - "+nbr;
    }
}
